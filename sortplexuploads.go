package main

import "fmt"
import "os"
import "strings"
import "io"

func main() {
    if len(os.Args) < 2 {
      fmt.Println("Directory argument missing!");
      return;
    }
    argDir := os.Args[1];
    fmt.Println("Open Directory...", argDir); 
    f,e := os.Open(argDir);
    if e != nil {
       panic(e);
    }
    infos,e := f.Readdir(0);
    if e != nil {
       panic(e);
    }

    filesToCopy := 1000;

    for _, val := range infos {
       name := val.Name();
       if len(name) < 15 {
          fmt.Println("This is not a valid picture:", name);
          continue;
       } 
       name = name[:10];
       meta := strings.Split(name,"-");
       if len(meta) != 3 {
          fmt.Println("This is not a valid picture:", val.Name());
          continue;
       }
       
//       fmt.Println("Year:",meta[0],"Month:",meta[1],"Day:",meta[2]);

       dirToCreate := "/media/disk2/Fotos/sortiert/" + meta[0] + "/" + meta[1];

       os.MkdirAll(dirToCreate, 0777);
	
	dst := dirToCreate + "/" + val.Name();
        src := argDir + "/" + val.Name();

       if filesToCopy > 0 {
           fmt.Println(src, "->", dst);
           Copy(dst, src);
           filesToCopy--;
       }

    }



}

func Copy(dst, src string) error {
    in, err := os.Open(src)
    if err != nil { 
	return err;
    }
    defer in.Close()
    out, err := os.Create(dst)
    if err != nil { 
        return err;
    }
    defer out.Close()
    _, err = io.Copy(out, in)
    cerr := out.Close()
    if err != nil { return err }
    return cerr
}

